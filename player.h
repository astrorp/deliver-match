#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <unordered_set>
#include <chrono>
#include "actionlist.h"

class Zone;
class Game;

class Player {
  Zone* base;
  std::unordered_set<Zone*> owned_zones;
  std::chrono::steady_clock::time_point tp_last_reset = std::chrono::steady_clock::now();
  int id;
public:
  Player(int id) : id(id), gold(0) {}
  virtual void broadcastInit(Game* g)=0;
  virtual void broadcastData(Game* g)=0;
  virtual BareActionList getActions()=0;
  ActionList getActions(Game* g);
  int gold;

  virtual ~Player() {}

  int getId();

  Zone* getBase();
  int getNumberOfZones();
  std::unordered_set<Zone*> getOwnedZones();

  // resets the timer
  void resetTimer();

  // returns in millisec the time to respond
  int getTimer();
};

#endif

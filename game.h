#ifndef _GAME_H_
#define _GAME_H_

#include <algorithm>
#include <vector>
#include <cassert>

class Player;
class ActionList;
class Zone;

class Game {
  Player* player1, *player2;
  bool player1_loses, player2_loses;
  std::vector<Zone*> zones;
  std::vector<std::pair<Zone*, Zone*>> links;
  int N, M;

  void step(Player *player, const ActionList& al);

  // must be (N+2)*(M+2) with the sides being false
  std::vector<std::vector<Zone*>> map;

  // generates a map which fills up map and zones
  void genMap(int N, int M, int Power);

  // Must be called only once
  void genLinks();

  void genBases();

public:

  void loop();

  std::vector<std::pair<Zone*, Zone*>> getLinks() { return links; }

  Zone* getZone(int i) { return zones.at(i); }

  // must be called only once, before init
  void setPlayer(int i, Player* p);

  Game();

  void init();
  bool ended();

  // checks that: `from` zones have enough units from the player
  // that `from` and `to` are adjacent
  bool valid(Player* player, const ActionList& actionlist);

  /** Must be called when at least 1 game-ending condition is true
    * Multiple game-ending condition can be true, so we must be careful
    * e.g. if one base is occupied, then that must determine the win condition
    */
  std::pair<int, int> getScore();

  void newTurn();

  int getNumberOfZones() {
    return zones.size();
  }

  Player* getPlayer(int id) {
    switch(id) {
      case 1: return player1;
      case 2: return player2;
      default: assert(0);
    }
  }

  const std::vector<Zone*>& getZones() { return zones; }

  int getNumberOfLinks() {
    return links.size();
  }
};

#endif

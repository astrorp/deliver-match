#include "player.h"
#include "exception.h"

ActionList Player::getActions(Game* g) {
  try {
    return getActions().applyGame(g);
  }
  catch (CannotConvertToActionException e) {
    throw InvalidOutputException("", this);
  }
}

int Player::getId() { return id; }

Zone* Player::getBase() {
  return base;
}
int Player::getNumberOfZones() {
  return owned_zones.size();
}
std::unordered_set<Zone*> Player::getOwnedZones() {
  return owned_zones;
}

// resets the timer
void Player::resetTimer() {
  tp_last_reset = std::chrono::steady_clock::now();
}
// returns in millisec the time to respond
int Player::getTimer() {
  auto now = std::chrono::steady_clock::now();
  return std::chrono::duration_cast<std::chrono::milliseconds>(now - tp_last_reset).count();
}
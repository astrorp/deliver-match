#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

#include <stdexcept>
#include "io.h"

// The point of defining new exception classes instead of using std::exception
// or std::runtime_error is that we don't accidentally catch an exception
// that wasn't thrown by our code.

class InvalidOutputException : std::runtime_error {
public:
  InvalidOutputException(const char* message, Player *p) 
    : std::runtime_error(message), player(p) { }

  Player *player;
};

// Not sure if this long exception name fits the C++ conventions
// Feel free to rename if it you don't think so
class BothPlayersInvalidOutputException : std::runtime_error {
public:
  BothPlayersInvalidOutputException() 
    : std::runtime_error("Both players gave an invalid input") { }
};

class TimeOutException : std::runtime_error {
public:
  TimeOutException(Player *p)
    : std::runtime_error("A player has crossed the time limit."), player(p) { }

  Player *player;
};

class BothPlayersPlayerTimeOutException : std::runtime_error {
public:
  BothPlayersPlayerTimeOutException()
    : std::runtime_error("Both players has crossed the time limit.") { }
};

class CannotConvertToActionException : std::runtime_error {
public: 
  CannotConvertToActionException() : std::runtime_error("The BareAction object contains invalid data") { }
};

#endif

#ifndef _ZONE_H_
#define _ZONE_H_

#include <cassert>
#include <vector>
#include <unordered_set>

#include "player.h"

class Zone {
  Player* owner;
  int units1, units2;
  int id;
  int production;
  std::unordered_set<Zone*> adjacent_zones;
public:
  Zone(int id, int production) : owner(NULL), units1(0), units2(0), id(id), production(production) {}
  Player* getOwner() { return owner; }

  // must be called only on initializing
  void addAdjacentZone(Zone* zone) {
    adjacent_zones.insert(zone);
  }

  bool adjacent(Zone* zone) {
    return adjacent_zones.find(zone) != adjacent_zones.end();
  }
  
  int getUnits(Player* player) {
    switch(player->getId()) {
      case 1: return units1;
      case 2: return units2;
      default: assert(false);
    }
  }

  void setOwner(Player* p) {
     owner->getOwnedZones().erase(this);
     owner = p;
     owner->getOwnedZones().insert(this);
  }
  void setUnits(Player* p, int units) {
    switch(p->getId()) {
      case 1: units1 = units; break;
      case 2: units2 = units; break;
      default: assert(0);
    }
  }

  int getProduction() { return production; }
  
  int getId() { return id; }
};

#endif

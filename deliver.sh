#!/bin/bash
echo "Delivering new match" >&2
read image1
read image2
container1="$(docker create "$image1")"
container2="$(docker create "$image2")"
echo "Running $image1 against $image2 in containers $container1 and $container2 respectively" >&2
mkfifo player1_stdin player1_stdout player1_stderr player2_stdin player2_stdout player2_stderr
trap "rm player1_stdin player1_stdout player1_stderr player2_stdin player2_stdout player2_stderr" EXIT
docker start -a "$container1" 0<player1_stdin 1>player1_stdout 2>player1_stderr &
docker start -a "$container2" 0<player2_stdin 1>player2_stdout 2>player2_stderr &
./main; status=$?
docker kill "$container1" >/dev/null 2>&1 && echo "Container $container1 killed" >&2 || true
docker kill "$container2" >/dev/null 2>&1 && echo "Container $container1 killed" >&2 || true
if [ $status -ne 0 ]; then
    echo -e "\033[31mmain exited with status \033[1m$status\033[0m" >&2
fi

exit $status

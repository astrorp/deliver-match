#ifndef _IO_H_
#define _IO_H_

#include <iostream>
#include "player.h"
#include <cassert>
#include <fstream>

class Game;

class IoPlayer : public Player {
  std::ostream& in;
  std::istream& out, &err;
public:
  IoPlayer(int id, std::ostream& in, std::istream& out, std::istream& err) : Player(id), in(in), out(out), err(err) {}
  virtual void broadcastInit(Game* g);
  virtual void broadcastData(Game* g);
  virtual BareActionList getActions();
  virtual ~IoPlayer() {}
};

class Logger {
  std::ostream& os;
  const std::string logname;
  // mutex? not really
  // flush! yupp yupp
public:
  Logger(const std::string& logname, std::ostream& os) : os(os), logname(logname) {}

  void dataStart();
  void dataEnd();

  template <typename T>
  Logger& operator<<(const T& t) {
    dataStart();
    os << t;
    dataEnd();
    return *this;
  }
};

class Io {
  std::ofstream player1_stdin, player2_stdin;
  std::ifstream player1_stdout, player2_stdout, player1_stderr, player2_stderr;
  // result will contain two number upon termination: the results
  // log_private will collect server data
  // log_public will collect game data and players' stderr output
  std::ofstream result_output, log_private_output, log_public_output;
public:
  Io();
  IoPlayer* genPlayer(int id);
  void dumpResult(const std::pair<int, int>&);
  // error, warning should be redirected to a normal file which cannot be read by players
  Logger log_error, log_warning, log_game;
};

#endif

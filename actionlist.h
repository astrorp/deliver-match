#ifndef _ACTIONLIST_H_
#define _ACTIONLIST_H_

#include <unordered_map>
#include "game.h"

class Zone;

struct Action;
struct BareAction {
  int number, from, to;
  BareAction(int number, int from, int to) : number(number), from(from), to(to) {};
  Action applyGame(Game* g);
};
struct Action {
  int number;
  Zone* from, *to;
  Action(int number, Zone* from, Zone* to) : number(number), from(from), to(to) {};
  operator BareAction();
};

class BareActionList : public std::vector<BareAction> {
public:
  BareActionList() {}
  ActionList applyGame(Game* g);
};

class ActionList : public std::vector<Action> {
public:
  ActionList() {}
  operator BareActionList();
};

#endif

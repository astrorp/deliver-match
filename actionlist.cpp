#include "actionlist.h"

#include "game.h"
#include "zone.h"
#include <algorithm>
#include "exception.h"

Action BareAction::applyGame(Game* g) {
  if (from < 0 || to < 0 || from >= g->getNumberOfZones() || to >= g->getNumberOfZones())
    throw CannotConvertToActionException();

  return Action(number, g->getZone(from), g->getZone(to));
}

Action::operator BareAction() {
  return BareAction(number, from->getId(), to->getId());
}

ActionList BareActionList::applyGame(Game* g) {
    ActionList result;
    std::transform(begin(), end(), std::back_inserter(result), [g](BareAction it) -> Action { return it.applyGame(g); });
    return result;
}

ActionList::operator BareActionList() {
    BareActionList result;
    std::transform(begin(), end(), std::back_inserter(result), [](Action it) -> BareAction { return it; });
    return result;
}

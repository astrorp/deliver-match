#include <cassert>
#include <queue>
#include "game.h"
#include "player.h"
#include "zone.h"
#include "actionlist.h"
#include "exception.h"

using std::pair;

// Game::init() must be called before loop()
void Game::loop() {
  try {

    for (int turn_number = 0; turn_number < 500; turn_number++) {
      newTurn();
      if (ended())
        return;
    }

  } catch (InvalidOutputException e) { // TODO: log

    switch (e.player->getId()) {
      case 1:
        player1_loses = true;
        return;
      case 2:
        player2_loses = true;
        return;
    }

  } catch (TimeOutException e) {

    switch (e.player->getId()) {
      case 1:
        player1_loses = true;
        return;
      case 2:
        player2_loses = true;
        return;
    }

  } catch (BothPlayersInvalidOutputException) {
    player1_loses = true;
    player2_loses = true;
  } catch (BothPlayersPlayerTimeOutException) {
    player1_loses = true;
    player2_loses = true;
  } 
}

void Game::setPlayer(int i, Player* p) {
  assert(i == 1 || i == 2);
  if (i == 1) player1 = p;
  if (i == 2) player2 = p;
}

Game::Game() : 
  player1(NULL), 
  player2(NULL), 
  player1_loses(false),
   player2_loses(false) { }

void Game::genBases() {
  int base1id = rand()%getNumberOfZones();
  int base2id;
  do {
    base2id = rand()%getNumberOfZones();
  } while (base1id == base2id);

  Zone* base1 = getZone(base1id);
  Zone* base2 = getZone(base2id);

  base1->setOwner(player1);
  base1->setUnits(player1, 10);

  base2->setOwner(player2);
  base2->setUnits(player2, 10);
}

void Game::init() {
  genMap(20, 20, 40);
  genLinks();

  genBases();

  // this must be after any initialization:
  player1->broadcastInit(this);
  player2->broadcastInit(this);
}

void Game::genLinks() {
  assert(links.empty());
  for (int i = 1; i <= N; i++) {
    for (int j = 1; j <= M; j++) {
      if (map[i][j] == NULL)
        continue;
      Zone* z1 = map[i][j];
      if (map[i][j+1] != NULL) {
        Zone* z2 = map[i][j+1];
        links.push_back({z1,z2});
        z1->addAdjacentZone(z2);
        z2->addAdjacentZone(z1);
      }
      if (map[i+1][j] != NULL) {
        Zone* z2 = map[i+1][j];
        links.push_back({z1,z2});
        z1->addAdjacentZone(z2);
        z2->addAdjacentZone(z1);
      }
    }
  }
}

int getRandomProduction() {
  if (rand()%2)
    return 0;
  if (rand()%2)
    return rand()%3;
  return rand()%7;
}

void Game::genMap(int N, int M, int Power) {
  this->N = N;
  this->M = M;
  assert(Power>0);
  map.resize(N+2);
  for (int i = 0; i < N+2; i++) {
    map[i].resize(M+2);
    for (int j = 0; j < M+2; j++)
      map[i][j] = NULL;
  }

  assert(zones.size() == 0);
  zones.reserve((N+2)*(M+2));
  std::queue<std::pair<std::pair<int,int>, int>> K;
  auto fillZone = [&](int x, int y, int power){
    assert(x > 0 && y > 0 && x <= N && y <= M && map[x][y] == NULL);
    Zone* zone = new Zone(zones.size(), getRandomProduction());
    zones.push_back(zone);
    map[x][y] = zone;
    K.push({{x, y}, power});
  };

  fillZone(1+N/2, 1+M/2, Power);
  while (!K.empty()) {
    using std::tie;
    int x = K.front().first.first;
    int y = K.front().first.second;
    int k = K.front().second;
    K.pop();
    assert(k>=0);
    auto propagate = [&](int x, int y){
      if (x > 0 && y > 0 && x <= N && y <= M && map[x][y] == NULL) {
        fillZone(x, y, Power - rand()%5);
      }
    };
    propagate(x, y+1);
    propagate(x, y-1);
    propagate(x+1, y);
    propagate(x-1, y);
  }
}

// This function only returns true if the game has ended because a
// player captured the other player's base. (= non-exceptional beavior)
bool Game::ended() {
  if (player1->getBase()->getOwner() != player1)
    return true;
  if (player2->getBase()->getOwner() != player2)
    return true;
}

/** Multiple game-ending condition can be true, so we must be careful
  * If one base is occupied, then that must determine the win condition
  */
std::pair<int, int> Game::getScore() {
  std::pair<int, int> win1 = {2,0}, tie = {1,1}, win2 = {0,2};
  
  if (player1_loses && player2_loses) return tie;

  if (player1_loses) return win2;
  if (player2_loses) return win1;

  if (player1->getBase()->getOwner() != player1) {
    if (player2->getBase()->getOwner() != player2)
      return tie;
    return win2;
  } else if (player2->getBase()->getOwner() != player2)
    return win1;

  // If this is reached, we are over 500 turns, and neither players have captured
  // the other's base
  int zone1 = player1->getNumberOfZones();
  int zone2 = player2->getNumberOfZones();
  if (zone1 < zone2) return win2;
  if (zone2 > zone1) return win1;
  return tie;
}

void Game::step(Player *player, const ActionList& al) {

  for (Action a : al) {
    if (a.from->getUnits(player) < a.number)
      throw InvalidOutputException("Not enough units", player);

    if (a.number < 0)
      throw InvalidOutputException("Cannot move a negative number of units", player);

    if (!a.from->adjacent(a.to))
      throw InvalidOutputException("Cannot move units to a zone that is not a neighbour", player);

    a.from->setUnits(player, a.from->getUnits(player) - a.number);
      a.to->setUnits(player,   a.to->getUnits(player) + a.number);
  }

}

void Game::newTurn() {
  // Give them the data for the turn
  player1->broadcastData(this);
  player2->broadcastData(this);

  // Get moves, check time-out, check input, LOG?
  // TODO getActions should terminate on time-out
  // TODO i/o should be buffered so the player does not have to wait for us to get the answers

  player1->resetTimer();
  ActionList al1 = player1->getActions(this);
  int timer_player1 = player1->getTimer();

  player2->resetTimer();
  ActionList al2 = player2->getActions(this);
  int timer_player2 = player2->getTimer();

  if (timer_player1 > 100) {
    if (timer_player2 > 100) {
      throw BothPlayersPlayerTimeOutException();
    }
    else
      throw TimeOutException(player1);
  }
  else if (timer_player2 > 100)
      throw TimeOutException(player2);

  // Do the moves
  // This code makes sure that both players' output is processed,
  // so if both give invalid output, the game results in a tie.
  InvalidOutputException* player1_ex = nullptr;
  try 
    { step(player1, al1); }
  catch (InvalidOutputException e) 
    { player1_ex = &e; }
  
  try 
    { step(player2, al2); }
  catch (InvalidOutputException) {
    if (player1_ex == nullptr) // Only player2's output is invalid
      throw;
    else
      throw BothPlayersInvalidOutputException();
  }

  if (player1_ex != nullptr) // Only player1's output is invalid
    throw player1_ex;

  // Generate units and decrease gold
  // 10 gold -> 1 unit generated on base
  player1->getBase()->setUnits(player1, player1->gold / 10);
  player1->gold -= player1->gold / 10 * 10;
  player2->getBase()->setUnits(player2, player2->gold / 10);
  player2->gold -= player2->gold / 10 * 10;

  // Generate gold
  for (Zone *zone : player1->getOwnedZones())
    player1->gold += zone->getProduction();

  for (Zone *zone : player2->getOwnedZones())
    player2->gold += zone->getProduction();

  // Fight
  for (Zone *zone : this->getZones()) {
    int a = zone->getUnits(player1);
    int b = zone->getUnits(player2);

    if (a > 0 && b > 0)
    {
      if (a > 3 && b > 3) {
        zone->setUnits(player1, a - 3);
        zone->setUnits(player2, b - 3);
      }
      else if (a > b) {
        zone->setUnits(player1, a - b);
        zone->setUnits(player2, 0);
      }
      else {
        zone->setUnits(player1, 0);
        zone->setUnits(player2, b - a);
      }
    }
  }

  // Capture zones
  for (Zone *zone : this->getZones()) {
    if (zone->getUnits(player1) > 0 && zone->getUnits(player2) == 0) {
      zone->setOwner(player1);
    }
    if (zone->getUnits(player1) == 0 && zone->getUnits(player2) > 0) {
      zone->setOwner(player2);
    }
  }
}
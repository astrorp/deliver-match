#include <iostream>

#include "io.h"
#include <iomanip>
#include "game.h"
#include "zone.h"
#include "actionlist.h"

Io::Io():
  player1_stdin("player1_stdin"),
  player2_stdin("player2_stdin"),
  player1_stdout("player1_stdout"),
  player2_stdout("player2_stdout"),
  player1_stderr("player1_stderr"),
  player2_stderr("player2_stderr"),
  result_output("result"),
  log_private_output("log_private"),
  log_public_output("log_public"),
// ---
  log_error("ERROR", log_private_output),
  log_warning("WARNING", log_private_output),
  log_game("GAME", log_public_output)
{
  // if we need unnamed/numbered pipes
  // __gnu_cxx::stdio_filebuf fb(0, std::ios::in);
  // std::istream is(fb);
}

IoPlayer* Io::genPlayer(int id) {
  switch (id) {
    case 1: return new IoPlayer(1, player1_stdin, player1_stdout, player1_stderr);
    case 2: return new IoPlayer(2, player2_stdin, player2_stdout, player2_stderr);
    default: assert(0);
  }
}

void Io::dumpResult(const std::pair<int, int>& pair) {
  result_output << pair.first << " " << pair.second << std::endl;
}

void Logger::dataStart() {
  using namespace std::chrono;
  auto now = system_clock::now();
  auto in_time_t = system_clock::to_time_t(now);
  os << "[" << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X") << "] " << logname << ": ";
}

void Logger::dataEnd() {
  os << std::endl;
}

void IoPlayer::broadcastInit(Game *g) {
  in << getId() << " " << g->getNumberOfZones() << " " <<
    g->getPlayer(1)->getBase() << " " << 
    g->getPlayer(2)->getBase() << " " << 
    g->getNumberOfLinks() << std::endl;
  for (Zone* z: g->getZones()) {
    in << z->getProduction() << " ";
  }
  in << std::endl;
  for (std::pair<Zone*, Zone*> z: g->getLinks()) {
    in << z.first->getId() << " " << z.second->getId();
  }
  in << std::endl;
}

void IoPlayer::broadcastData(Game *g) {
  in << gold << std::endl;
  for (Zone *zone : g->getZones()) {
    in 
      << zone->getOwner()->getId() << " " 
      << zone->getUnits(g->getPlayer(1)) << " " 
      << zone->getUnits(g->getPlayer(2)) << std::endl; 
  }
}

BareActionList IoPlayer::getActions() {
  BareActionList result;

  while (true) {
    int number, from, to;
    out >> number;

    if (number == -1) // End of data
      break;

    out >> from >> to;
    result.push_back({number, from, to});
  }
  
  return result;
}